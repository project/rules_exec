INTRODUCTION
------------

The Rules Exec module provides a Rules action that allows you to run shell
commands from your own rules. The action will add variables with the command
output as a list, plus the command return value, allowing you to do further
checking and processing.


REQUIREMENTS
------------

This module requires no additional module to work (besides Rules, of
course).


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for
further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no
configuration. When enabled, the module will provide an "Execute a shell
command" Rules action under the "System" group, that will allow you to
execute a command.


USAGE
-----

    1. Add the "Execute a shell command" to a rule

    2. Input the command file path in the "Command" field. The path should
       be absolute, otherwise it will be taken relatively to your Drupal
       root.

    3. Input any command line arguments in the "Arguments" field, one per
       line. You do not need to quote and/or escape any shell character, as
       the module will take care of doing that.

## WARNING ##

Be extra careful when passing user supplied data to the action provided by
this module.

First of all, you should NEVER use any token substitution value in the
"Command" field, unless you know what you're doing.

For the command arguments, be extra careful to not allow user input to alter
the command behaviour unintentionally, or maliciously. Most of times this
involves protecting the command from interpreting user input beginning with
"-" as a command line argument.

For most Unix commands, you can add a "--" parameter to separate your
hard-coded arguments from user data. For example, this is WRONG:

    Command: /bin/df
    Arguments:
      -h
      /var/lib/template-data.tar
      [user-supplied-data]

This is RIGHT:

    Command: /bin/tar
    Arguments:
      -xf
      /var/lib/template-data.tar
      --
      [user-supplied-data]

But not all commands work the same. Here's a WRONG example for grep(1):

    Command: /bin/grep
    Arguments:
      [user-supplied-data]
      /var/lib/my-data.txt

The RIGHT version:

    Command: /bin/grep
    Arguments:
      -e
      [user-supplied-data]
      /var/lib/my-data.txt

Please, carefully read the manual page for the command being executed to
know how to avoid input arguments from being interpreted as command line
switches.
