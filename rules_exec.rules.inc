<?php
/**
 * @file
 * Rules hooks for the Rules Exec module.
 */

/**
 * Implements hook_rules_action_info().
 */
function rules_exec_rules_action_info() {
  return array(
    'rules_exec_exec' => array(
      'label' => t('Execute a shell command'),
      'group' => t('System'),
      'parameter' => array(
        'command' => array(
          'type' => 'text',
          'label' => t('Command'),
          'default mode' => 'input',
          'ui class' => 'RulesExecUICommand',
          'description' => t('Path to the command binary, <strong>without arguments</strong>. Non-absolute paths will be relative to %drupal_root.',
                             array('%drupal_root' => DRUPAL_ROOT)),
        ),
        'arguments' => array(
          'type' => 'list<text>',
          'label' => t('Arguments'),
          'default mode' => 'input',
          'description' => t('The command arguments, one per line. There is no need to quote arguments — this will be done automatically.'),
          'optional' => TRUE,
        ),
      ),
      'provides' => array(
        'output' => array(
          'label' => 'Output',
          'type' => 'list<text>',
        ),
        'return_code' => array(
          'label' => 'Return code',
          'type' => 'integer',
        ),
      ),
    ),
  );
}

/**
 * Small UI class to show command input as a textfield.
 */
class RulesExecUICommand extends RulesDataUIText {
  /**
   * Override parent method to change form element type.
   */
  public static function inputForm($name, $info, $settings, RulesPlugin $element) {
    $form = parent::inputForm($name, $info, $settings, $element);
    $form[$name]['#type'] = 'textfield';
    return $form;
  }
}
